# ChectLabelFix

This is a fix for an existing modification [Chest Marking System](https://gitlab.com/speeder1/ChestNameWithHoverLabel)

Now changing Zoom doesn't break the interface.
# INSTRUCTION
Download [the zip file](https://gitlab.com/MrMult/chectlabelfix/-/blob/main/ChestLabelFix%201.7.0.zip) and extract it to the Mods folder
